## Forgot Password Block

Module provides a quick and simple block to display the "Forgot Password"
form.

### Requirements

No special requirements. Drupal User and Block modules are all that is
required and is already a part of core.

### Install

Install module like any other contributed module.

```bash
composer require drupal/forgot_password_block
```

### Usage

* Goto "Block Layout" page.
* Select region and find the "Forgot Password" block.
* Select and configure the block.
* Assign to region and save.
* Now block with "Forgot Password" form should now be appearing.

### Maintainers

George Anderson (geoanders)
https://www.drupal.org/u/geoanders
